<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Global Settings.
 *
 * Admin area settings for all instances of the block
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

global $DB;

$blockfolder = new admin_category('block_laixmo_folder', get_string('pluginname', 'block_laixmo'), false);

$ADMIN->add('blocksettings', $blockfolder);

$settings->visiblename = get_string('settings', 'block_laixmo');

$ADMIN->add('block_laixmo_folder', $settings);
$ADMIN->add('block_laixmo_folder', new admin_externalpage('block_laixmo_modules',
    get_string('modules', 'block_laixmo'), new moodle_url('/blocks/laixmo/settings-modules.php')));

if ($ADMIN->fulltree) {
    $settings->add(new admin_setting_heading(
        'serverconfig',
        get_string('serverconfig', 'block_laixmo'),
        get_string('serverconfig_desc', 'block_laixmo')
    ));

    $settings->add(new admin_setting_configtext(
        'laixmo/Server_URL',
        get_string('server_url_label', 'block_laixmo'),
        get_string('server_url_desc', 'block_laixmo'),
        ''
    ));

    $settings->add(new admin_setting_configtext(
        'laixmo/Laixmo_TOKEN',
        get_string('server_token_label', 'block_laixmo'),
        get_string('server_token_desc', 'block_laixmo'),
        ''
    ));
}

$settings = null;