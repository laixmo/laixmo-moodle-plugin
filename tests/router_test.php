<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Unit Tests for routes
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use block_laixmo\local\handler\default_handler;
use block_laixmo\local\routing\route;
use block_laixmo\local\routing\router;

defined('MOODLE_INTERNAL') || die();


/**
 * @group block_laixmo
 */
final class block_laixmo_router_testcase extends advanced_testcase {

    public function test_empty_router() {
        $router = new router([]);

        $route = $router->get_active_route('\\block_laixmo\\local\\handler\\default_handler');

        $this->assertInstanceOf('\\block_laixmo\\local\\routing\\route', $route);
        $this->assertInstanceOf('\\block_laixmo\\local\\handler\\default_handler', $route->handler);
    }

}