<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Unit Tests for routes
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;

require_once("{$CFG->libdir}/filelib.php");

use block_laixmo\local\handler\default_handler;
use block_laixmo\local\routing\route;

/**
 * @group block_laixmo
 */
final class block_laixmo_proxy_testcase extends advanced_testcase
{

    public function test_module_list() {
        curl::mock_response(json_encode([
            'modules' => ['A', 'B', 'C']
        ]));

        $modules = \block_laixmo\proxy::get_available_modules();

        $this->assertEquals(3, count($modules));
    }

    public function test_module_response() {
        curl::mock_response(json_encode([
            "title" => "Unit Test",
            "output" => [
                ['type' => 'table']
            ]
        ]));

        $response = \block_laixmo\proxy::module('unit', 'test', []);

        $this->assertEquals('Unit Test', $response->title);
        $this->assertEquals(1, count($response->outputs));
    }
}