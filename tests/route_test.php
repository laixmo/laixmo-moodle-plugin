<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Unit Tests for routes
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

use block_laixmo\local\handler\default_handler;
use block_laixmo\local\routing\route;

/**
 * @group block_laixmo
 */
final class block_laixmo_route_testcase extends advanced_testcase {

    public function test_static_match() {
        $route = new route('/test', '\\block_laixmo\\local\\handler\\default_handler');

        $this->assertTrue($route->does_match('/test'));
        $this->assertFalse($route->does_match('/test2'));
    }

    public function test_param_match() {
        $route = new route('/param/:param1', '\\block_laixmo\\local\\handler\\default_handler');

        $this->assertTrue($route->does_match('/param/1'));
    }

    public function test_multiple_params() {
        $route = new route('/:param1/:param2', '\\block_laixmo\\local\\handler\\default_handler');

        $route->parse_params('/0/asdf');

        $this->assertEquals('0', $route->params->get('param1'));
        $this->assertEquals('asdf', $route->params->get('param2'));
    }

    public function test_create_url() {
        $route = new route('/:param1/:param2', '\\block_laixmo\\local\\handler\\default_handler');

        $route->parse_params('/0/asdf');

        $this->assertEquals('/0/asdf', $route->get_url());
    }
}