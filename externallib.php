<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Functions for use as WebService.
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once("{$CFG->libdir}/externallib.php");

use block_laixmo\proxy;

class block_laixmo_external extends external_api {

    public static function proxy_parameters() {
        return new external_function_parameters(
            [
                'module' => new external_value(PARAM_TEXT, 'Module to run'),
                'output' => new external_value(PARAM_TEXT, 'Output of the Module'),
                'params' => new external_value(PARAM_TEXT, 'Parameters for this request'),
                'filter' => new external_value(PARAM_TEXT, 'Filter for this request'),
            ]
        );
    }

    public static function proxy_returns() {
        return
            new external_single_structure(
                [
                    'output' => new external_value(PARAM_TEXT, 'Output')
                ]
        );
    }

    public static function proxy($module, $output, $params, $filter) {

        $body = json_decode(json_decode($params, true), true);

        $body['filter'] = json_decode($filter, true);

        parse_str($filter, $body['filter']);

        $ret = proxy::module($module, $output, $body);

        return $ret->to_ajax_response();
    }

    public static function output_list_parameters() {
        return new external_function_parameters(
            [
                'module' => new external_value(PARAM_TEXT, 'Module to get Outputs for.')
            ]
        );
    }

    public static function output_list_returns() {
        return new external_multiple_structure(
                new external_single_structure(
                    [
                        'key' => new external_value(PARAM_TEXT, 'Output Key'),
                        'name' => new external_value(PARAM_TEXT, 'Output Name')
                    ]
                )
        );
    }

    public static function output_list($module) {
        global $DB;

        $outputs = $DB->get_records('block_laixmo_module_outputs', ['module_key' => $module, 'output_type' => 'block']);

        return array_map(function($e) {
            return [
                'key' => $e->output_key,
                'name' => $e->description
            ];
        }, $outputs);
    }
}