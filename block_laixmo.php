<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Main Entry Point
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use block_laixmo\local\user;

defined('MOODLE_INTERNAL') || die();

class block_laixmo extends block_base
{

    /**
     * @var renderer_base
     */
    private $renderer;

    public function init() {
        $this->title = get_string('block_title', 'block_laixmo');
    }

    public function has_config() {
        return true;
    }

    public function applicable_formats() {
        return array(
            'my' => false,
            'site-index' => true,
            'course-*' => true,
        );
    }

    /**
     * @return stdClass|stdObject
     * @throws coding_exception
     * @throws moodle_exception
     */
    public function get_content() {
        global $PAGE, $USER, $COURSE;

        $this->renderer = $this->page->get_renderer('block_laixmo');

        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass();

        $configured = isset($this->config);

        if (!$configured) {
            $this->render_error(get_string('configure', 'block_laixmo'));
        } else {
            $config = null;
            $params = [
                'userid' => $USER->id
            ];

            if ($PAGE->pagetype == 'site-index') {
                $user = user::get();
                // If we are on the index page we need to get all courses for the User.
                // We will use the manager config if we find a single course in that we have at least teaching role.
                $params['courseids'] = $user->get_courses();

                if ($user->get_group() == user::GROUP_TEACHER) {
                    $config = $this->config->output_teacher;
                } else {
                    $config = $this->config->output_student;
                }
            } else {
                $params['courseids'] = [$COURSE->id];

                if (has_capability('block/laixmo:seeteacher', context_course::instance($COURSE->id))) {
                    $config = $this->config->output_teacher;
                } else {
                    $config = $this->config->output_student;
                }
            }

            if ($config !== null && count($params['courseids']) > 0) {
                $config = json_decode(base64_decode($config));
                $this->render_module($config->module, $config->output, $params);
            } else {
                $this->render_error(get_string('error:no_statistic', 'block_laixmo'));
            }
        }

        return $this->content;
    }

    private function render_error($error) {
        try {
            $this->content->text = $this->renderer->render_from_template('block_laixmo/base', [
                'content' => $error
            ]);
        } catch (\Exception $ex) {
            $this->context->text = "Error loading block";
        }
    }

    /**
     * Render the given Module to the page
     * @param string $module Module to render
     * @param string $output Module output to render
     * @param array[] $params Parameters
     * @throws coding_exception
     * @throws moodle_exception
     */
    private function render_module($module, $output, $params = []) {
        global $PAGE;

        if (\block_laixmo\output::exists($module, $output)) {
            if ($PAGE->pagetype == 'site-index') {
                $details = new moodle_url("/blocks/laixmo/index.php/statistics/{$module}");
            } else {
                $details = new moodle_url("/blocks/laixmo/index.php/course/{$PAGE->course->id}/{$module}");
            }

            $this->content->text = $this->renderer->render_from_template('block_laixmo/base', [
                'content' => get_string('loading', 'block_laixmo'),
                'details' => $details
            ]);

            $PAGE->requires->js_call_amd('block_laixmo/connector', 'block', [
                'instance' => $this->context->instanceid,
                'module' => $module,
                'output' => $output,
                'params' => json_encode($params)
            ]);

        } else {
            // The selected output does not exist anymore.
            $this->content->text = $this->renderer->render_from_template('block_laixmo/base', [
                'content' => get_string('not_available', 'block_laixmo')
            ]);
        }

    }
}