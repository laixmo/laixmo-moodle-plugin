<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Instance configuration.
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Block instance configuration
 */
class block_laixmo_edit_form extends block_edit_form
{

    /**
     * @param object $mform Moodle Form to extend
     * @throws coding_exception
     */
    protected function specific_definition($mform) {

        global $PAGE;

        $target = 'course';

        if ($PAGE->pagetype == 'site-index') {
            $target = 'global';
        }

        $outputs = \block_laixmo\output::get('block', $target);

        $modules = ['student' => [], 'teacher' => []];

        foreach ($outputs as $output) {

            foreach ($output->get_groups() as $group) {
                $modules[$group][$output->get_key()] = $output->get_name();
            }
        }

        $default = reset($modules);

        $mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));

        $mform->addElement('select', 'config_output_student', get_string('config_student', 'block_laixmo'), $modules['student']);
        $mform->setDefault('config_output_student', $default);
        $mform->setType('config_output_student', PARAM_RAW);

        $mform->addElement('select', 'config_output_teacher', get_string('config_teacher', 'block_laixmo'), $modules['teacher']);
        $mform->setDefault('config_output_teacher', $default);
        $mform->setType('config_output_teacher', PARAM_RAW);

        $PAGE->requires->js_call_amd('block_laixmo/config', 'init', []);
    }
}