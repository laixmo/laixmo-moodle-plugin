/**
 * Moodle to Laixmo connector
 * @module block_laixmo/connector
 */
define(['core/ajax', 'core/url', 'jquery'], function(ajax, url, $) {
    return {
        getOutputs: function(module, output, params, filter) {

            if (filter === undefined) {
                filter = "";
            }

            return $.Deferred(function(deferred) {
                var request = ajax.call([
                    {
                        methodname: 'block_laixmo_proxy',
                        args: {
                            module: module,
                            output: output,
                            params: JSON.stringify(params),
                            filter: filter,
                        },
                    },
                ])[0];

                request.done(function(response) {
                    response = JSON.parse(response.output);
                    response = JSON.parse(response);

                    deferred.resolve(response);
                });
            });
        },

        render: function(output) {
            var self = this;

            /**
             *
             * @param {jQuery.Deferred} deferred
             * @return {Function}
             */
            function handle(deferred) {
                return function(handler) {
                    deferred.resolve(handler.handle(output));
                };
            }

            return $.Deferred(function(deferred) {
                if (self.SUPPORTED_TYPES.indexOf(output.type) >= 0) {
                    require(['block_laixmo/' + output.type + '-lazy'], handle(deferred));
                } else {
                    deferred.reject('error');
                }
            });
        },

        /**
         * Display types this connector version supports
         */
        SUPPORTED_TYPES: [
            'plot',
            'html',
        ],
    };
});