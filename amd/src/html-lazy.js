/**
 * @module block_laixmo/plot-lazy
 */
define([], function() {
    return /** @alias module:block_laixmo/plot-lazy */{
        /**
         * Apply module to the block
         * @param {object} data
         * @param {HTMLElement} content
         */
        block: function(data, content) {
            content.innerHTML = data.data;
        },

        handle: function(output) {
            return {
                html: output.data,
            };
        },

        /**
         * Initialize details page
         * @param {object} response
         */
        details: function() {
            // TODO: Implement.
        },
    };
});