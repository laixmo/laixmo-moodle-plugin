/**
 * @module block_laixmo/plot-lazy
 */
define(['block_laixmo/plotly', 'block_laixmo/api', 'jquery'], function(Plotly, api, $) {
    return /** @alias module:block_laixmo/plot-lazy */{
        /**
         * Apply module to the block
         * @param {object} data
         * @param {object} content
         */
        block: function(data, content) {
            var plotParams = {
                displayModeBar: false,
            };

            var plotLayout = {
                margin: {
                    t: 5,
                    b: 30,
                    l: 30,
                    r: 30,
                },
            };

            if (data.layout) {
                plotLayout = Object.assign(plotLayout, data.layout);
            }

            Plotly.newPlot(content, data.data, plotLayout, plotParams);

            window.onresize = function() {
                Plotly.Plots.resize(content);
            };
        },

        init: function(div, data) {
            var plotParams = {
                displayModeBar: false,
            };

            var plotLayout = {};

            if (data.layout) {
                plotLayout = Object.assign(plotLayout, data.layout);
            }

            Plotly.newPlot(div, data.data, plotLayout, plotParams);

            window.onresize = function() {
                Plotly.Plots.resize(div);
            };
        },

        handle: function(output) {
            var self = this;

            var node = $('<div />');

            return {
                html: node,

                js: function() {
                    self.init(node.get(0), output);
                },
            };
        },

        /**
         * Initialize details page
         * @param {object} response
         */
        details: function(response) {
            var plotParams = {
                displayModeBar: false,
            };

            var plotLayout = {};

            if (response.layout) {
                plotLayout = Object.assign(plotLayout, response.layout);
            }

            Plotly.newPlot('chart-area-' + response.id, response.data, plotLayout, plotParams);
        },
    };
});