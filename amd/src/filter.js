/**
 * Moodle to Laixmo connector
 * @module block_laixmo/connector
 */
define(['core/url', 'jquery', 'block_laixmo/api'], function(url, $, api) {
    return {
        init: function(course, module, output, params) {
            $(function() {
                var filter = $('#laixmo-filter-box').find('form');
                var feedback = $('#laixmo-filter-feedback');

                filter.on('submit', function(e) {
                    e.preventDefault();

                    feedback.empty();

                    var validation = $.Event('laixmo.form.validation');
                    filter.trigger(validation);

                    if (!validation.isDefaultPrevented()) {
                        filter.trigger('laixmo.form.validated');
                    }
                });

                filter.on('laixmo.form.validated', function() {
                    var formData = $(this).serialize();

                    api.getOutputs(module, output, params, formData).done(function(outputs) {
                        var promises = [];

                        for (var i = 0; i < outputs.length; i++) {
                            promises.push(api.render(outputs[i]));
                        }

                        $.when.apply($, promises)
                            .done(function() {
                                var scripts = [];
                                var content = $('#laixmo-content');

                                content.empty();

                                for (var i = 0; i < arguments.length; i++) {
                                    content.append(arguments[i].html);

                                    if (arguments[i].js !== undefined) {
                                        scripts.push(arguments[i].js);
                                    }
                                }

                                for (var x = 0; x < scripts.length; x++) {
                                    scripts[x]();
                                }
                            });
                        // TODO: Add error handling for Failed ajax request #10.
                    });
                });
            });
        },
    };
});