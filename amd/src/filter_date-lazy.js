/**
 * @module block_laixmo/plot-lazy
 */
define(['jquery'], function($) {
    return {
        init: function() {
            var filter = $('#laixmo-filter-box').find('form');
            var feedback = $('#laixmo-filter-feedback');

            filter.on('laixmo.form.validation', function(event) {
                var filters = filter.find('.laixmo-filter-date');

                $.each(filters, function(i, e) {
                    var from = $(e).find('input[data-date="from"]');
                    var to = $(e).find('input[data-date="to"]');

                    if (from.val() && to.val()) {
                        var fDate = Date.parse(from.val());
                        var tDate = Date.parse(to.val());

                        if (fDate > tDate) {
                            event.preventDefault();
                            $(e).addClass('has-danger');
                            feedback.append($('<p>Error in Date range, `from` date is after `to` date.</p>'));
                        } else {
                            $(e).removeClass('has-danger');
                        }

                    } else if (!from.val() !== (to.val() === '')) {
                        event.preventDefault();
                        $(e).addClass('has-danger');
                        feedback.append($('<p>Error in Date range, both dates must be given.</p>'));
                    } else {
                        $(e).removeClass('has-danger');
                    }
                });
            });
        },
    };
});