/**
 * Moodle to Laixmo connector
 * @module block_laixmo/connector
 */
define(
    ['core/ajax', 'core/notification', 'core/str', 'core/url', 'jquery', 'block_laixmo/api'],
    function(ajax, notification, strings, url, $, api) {

        /**
         * @exports block_laimox/connector
         */
        var connector = {
            /**
             *  Display contents of the module inside of the block
             *
             * @param {number} instance Instance ID
             * @param {string} module Module Key
             * @param {string} output Output key identifier
             * @param {object} params
             */
            block: function(instance, module, output, params) {
                var $html = $('#inst' + instance);
                var $content = $('.laixmo_content', $html);

                api.getOutputs(module, output, params)
                    .done(function(outputs) {
                        if (outputs.length > 0) {
                            for (var i = 0; i < outputs.length; i++) {
                                var res = outputs[i];

                                if (api.SUPPORTED_TYPES.indexOf(res.type) >= 0) {
                                    // eslint-disable-next-line no-loop-func
                                    require(['block_laixmo/' + res.type + '-lazy'], function(handler) {
                                        $content.empty();
                                        handler.block(res, $content.get(0));

                                        if (res.title !== undefined) {
                                            $('#instance-' + instance + '-header').text(res.title);
                                        }
                                    });
                                } else {
                                    strings.get_strings([
                                        {'key': 'type_not_supported', component: 'block_laixmo'},
                                    ]).done(function(s) {
                                        $content.text(s[0]);
                                    });
                                }
                            }
                        }
                    })
                    .fail(function(ex) {
                        strings.get_strings([
                            {'key': ex.errorcode, component: 'block_laixmo'},
                        ]).done(function(s) {
                            $content.text(s[0]);
                        });
                    });
            },

            /**
             * Initialize the Details Page
             */
            details: function() {
                /* global response */
                if (connector.types.indexOf(response.output.type) >= 0) {
                    require(['block_laixmo/' + response.type + '-lazy'], function(handler) {
                        handler.details(response);
                    });
                } else {
                    // TODO: Error Handling https://git.rwth-aachen.de/laixmo/laixmo-moodle-plugin/issues/10
                }
            },
        };

        return connector;
    });