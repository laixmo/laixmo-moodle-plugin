/**
 * Config javascript
 *
 * @module block_laixmo/config
 */
define(['core/ajax', 'jquery'], function(ajax, $) {

    return /** @alias module:block_laixmo/config */ {
        /**
         * Initialize the Configs page
         */
        init: function() {
            var $select = $('select[name=config_module]');
            var $outputs = $('select[name=config_output]');

            $select.on('change', function() {
                $outputs.empty();

                var request = ajax.call([
                    {
                        methodname: 'block_laixmo_output_list',
                        args: {
                            module: $select.val(),
                        },
                    },
                ])[0];

                request.done(function(response) {

                    for (var i = 0; i < response.length; i++) {
                        $outputs.append($('<option></option>')
                            .attr('value', response[i].key)
                            .text(response[i].name));
                    }
                }).fail(function() {
                    // TODO: Error handling https://git.rwth-aachen.de/laixmo/laixmo-moodle-plugin/issues/10
                });
            });
        },
    };
});