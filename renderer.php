<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Custom renderer.
 *
 * Renderer for Laixmo specific widgets.
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

use block_laixmo\local\filtering\filter;
use block_laixmo\output;
use block_laixmo\output\html;
use block_laixmo\output\plot;
use block_laixmo\output\table;

class block_laixmo_renderer extends plugin_renderer_base
{

    /**
     * @param $course string
     * @param $outputs OutputList
     * @return string
     * @throws moodle_exception
     */
    public function render_module_selection($course, $outputs) {
        return $this->render_from_template('block_laixmo/module_selection', [
            'options' => $outputs,
            'course' => $course,
        ]);
    }

    /**
     * Render Module Selection with optional Course Selection
     * @param $outputs output[] List of available outputs
     * @param $courses array List of Moodle Courses
     * @return bool|string
     * @throws moodle_exception
     */
    public function render_global_module_selection($outputs, $courses) {
        $courses = array_values($courses);

        return $this->render_from_template('block_laixmo/module_selection_global', [
            'outputs' => $outputs,
            'courses' => $courses,
        ]);
    }

    /**
     * @param filter[] $filter
     * @return string
     * @throws moodle_exception
     */
    public function render_filter($filter) {
        global $PAGE;

        if (count($filter) >= 0) {
            $content = '';

            foreach ($filter as $option) {
                $content .= $this->render_from_template(
                    "block_laixmo/filter_{$option->get_type()}",
                    $option->get_params()
                );

                $option->init($PAGE);
            }

            try {
                return $this->render_from_template('block_laixmo/filter_box', [
                    'filter' => $content,
                ]);
            } catch (moodle_exception $e) {
                return 'Error rendering Filter Menu.';
            }
        }
        return '';
    }

    /**
     * Render List of Outputs
     *
     * @param  output[] $list
     */
    public function render_output_list($list) {
        echo "<div id='laixmo-content'>";
        foreach ($list as $out) {
            $type = $out['type'];
            $class = '\\block_laixmo\\output\\' . $type;

            echo $this->render(new $class((object)$out));
        }
        echo "</div>";
    }

    /**
     * Render HTML Output
     * @param html $html
     * @return string
     */
    protected function render_html(html $html) {
        return $this->output->container($html->content);
    }

    /**
     * Render Table Outout
     * @param table $table
     * @return bool|string
     *
     * @throws moodle_exception
     */
    protected function render_table(table $table) {
        return $this->render_from_template('block_laixmo/table', [
            'head' => $table->head,
            'rows' => $table->rows,
        ]);
    }

    /**
     * Render Plot output
     *
     * @param plot $plot
     * @return bool|string
     * @throws moodle_exception
     */
    protected function render_plot(plot $plot) {
        $data = $plot->get_data();

        return $this->render_from_template('block_laixmo/plot', [
            'data' => $data,
            'id' => uniqid(),
        ]);
    }
}