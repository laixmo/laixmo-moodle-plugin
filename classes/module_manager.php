<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Output Container
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo;

use Exception;
use stdClass;

defined('MOODLE_INTERNAL') || die();

class module_manager
{
    public static function update_available() {
        global $DB;

        try {
            $transaction = $DB->start_delegated_transaction();

            $DB->delete_records('block_laixmo_modules');
            $DB->delete_records('block_laixmo_module_outputs');
            $DB->delete_records('block_laixmo_output_groups');
            $DB->delete_records('block_laixmo_output_targets');

            $list = \block_laixmo\proxy::get_available_modules();

            foreach ($list as $module) {
                $obj = new stdClass;

                $obj->name = $module['key'];
                $obj->description = 'Lore Ipsum'; // The info does currently not give a description for the module.
                $obj->module_key = $module['key'];

                $DB->insert_record('block_laixmo_modules', $obj, false);

                foreach ($module['output'] as $output) {
                    $o = new stdClass;

                    $o->output_type = $output['display'];
                    $o->module_key = $module['key'];
                    $o->name = json_encode($output['name']);
                    $o->output_key = $output['key'];

                    $DB->insert_record('block_laixmo_module_outputs', $o, false);

                    if (!isset($output['groups'])) {
                        $output['groups'] = [];
                    }

                    foreach ($output['groups'] as $group) {
                        $g = new stdClass();

                        $g->output_key = $module['key'] . '-' . $output['key'];
                        $g->group_name = $group;

                        $DB->insert_record('block_laixmo_output_groups', $g, false);
                    }

                    if (!isset($output['targets'])) {
                        $output['targets'] = [];
                    }

                    foreach ($output['targets'] as $target) {
                        $t = new stdClass();

                        $t->output_key = $module['key'] . '-' . $output['key'];
                        $t->target_name = $target;

                        $DB->insert_record('block_laixmo_output_targets', $t, false);
                    }
                }
            }

            $transaction->allow_commit();
        } catch (Exception $ex) {
            $transaction->rollback($ex);
        }
    }
}