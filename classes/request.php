<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Laixmo Server Response
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo;

defined('MOODLE_INTERNAL') || die();

use dml_exception;

class request
{
    /**
     * D
     *
     * @param $group
     * @param $module
     * @param $output
     * @return request_info
     */
    public static function get_request_info($group, $module, $output, $target) : request_info{
        $info = new request_info();

        $outputs = output::get('report', $target, $group);

        foreach ($outputs as $out) {

            if (self::is_active_output($out, $module, $output)) {
                $out->set_selected(true);
                $info->set_active_output($out);
            }

            $info->add_output($out);
        }

        if (empty($output) || $output == null) {
            $outs = $info->get_outputs($module);

            if (count($outs) > 0) {
                $info->set_active_output($outs[0]);
            }
        }

        if ($info->get_active_output() === null) {
            $info->set_active_output($info->get_outputs()[0]);
        }

        return $info;
    }

    /**
     * Check if the given Output is the currently active output
     *
     * @param $out output
     * @param $module string
     * @param $output string
     * @return bool
     */
    private static function is_active_output($out, $module, $output) : bool {
        return (
            $out->get_module() === $module &&
            $out->get_output() === $output
        );
    }
}