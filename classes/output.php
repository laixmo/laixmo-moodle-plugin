<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Output Container
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo;

defined('MOODLE_INTERNAL') || die();

use ArrayAccess;
use stdClass;

/**
 * Class Output
 * @package block_laixmo
 */
class output implements ArrayAccess
{

    /**
     * Module of this Output
     * @var string
     */
    protected $module;

    /**
     * Module Output of this Output
     * @var string
     */
    protected $output;

    /**
     * Selected Output
     * @var string
     */
    protected $selected = '';

    /**
     * Display Name
     * @var string
     */
    protected $name;

    /**
     * @var string[]
     */
    protected $targets = [];

    /**
     * @var string[]
     */
    protected $groups = [];

    /**
     * Unique Identifier
     * @var string
     */
    protected $key;

    /**
     * Output constructor.
     * @param $entry
     */
    public function __construct($entry) {
        $this->module = $entry->module;
        $this->output = $entry->output_key;
        $this->name = $this->parse_name($entry->name);
        $this->targets[] = $entry->target_name;
        $this->groups[] = $entry->group_name;

        $this->key = base64_encode(json_encode(['module' => $this->module, 'output' => $this->output]));
    }

    /**
     * Returns the Modules Output Name
     * @return string
     */
    public function get_output() : string {
        return $this->output;
    }

    /**
     * Returns the Module Name
     * @return string
     */
    public function get_module() : string {
        return $this->module;
    }

    /**
     * Returns the Key
     * @return string
     */
    public function get_key() : string {
        return $this->key;
    }

    /**
     * Set the selected
     * @param $mode
     */
    public function set_selected($mode) {
        $this->selected = $mode ? 'selected' : '';
    }

    /**
     * Returns the selected State
     * @return string
     */
    public function get_selected() : string {
        return $this->selected;
    }

    /**
     * Returns the display name of this output
     * @return string
     */
    public function get_name() : string {
        return $this->name;
    }

    /**
     * Return groups of this output
     * @return string[]
     */
    public function get_groups()  : array {
        return $this->groups;
    }

    /**
     * Return targets of this output
     * @return string[]
     */
    public function get_targets() : array {
        return $this->targets;
    }

    /**
     * Parse the name of an output
     * @param string|stdClass $name
     * @return string
     */
    protected function parse_name($name) : string {
        $lang = explode('_', current_language())[0];
        $name = json_decode($name);

        if (is_string($name)) {
            return $name;
        }

        if (isset($name->$lang)) {
            return $name->$lang;
        } else if (isset($name->en)) {
            return $name->en;
        } else {
            // TODO: fix this.
            return "missing-lang-string";
        }
    }

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset) {
        return property_exists($this, $offset);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset) {
        return $this->$offset;
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value) {
        $this->$offset = $value;
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset) {
        $this->$offset = null;
    }

    /**
     * Checks if the given Output is known to the System
     *
     * @param $module string Module
     * @param $output string Output
     * @return bool
     */
    public static function exists($module, $output) : bool {
        global $DB;
        try {
            return $DB->record_exists(
                'block_laixmo_module_outputs',
                [
                    'module_key' => $module,
                    'output_key' => $output
                ]
            );
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Return all available Outputs for the Target
     *
     * @param string $type
     * @param string $target
     * @param string $group
     * @return output[]
     */
    public static function get($type = '%', $target = '%', $group = '%', $module = '%') : array {
        global $DB;

        $query = <<<SQL
SELECT
  CONCAT(M.module_key, '-', O.output_key, '-', T.target_name,'-', G.group_name),
  CONCAT(M.name, ' / ', O.output_key) AS 'display',
  M.module_key as 'module',
  O.*,
  T.target_name as 'target_name',
  G.group_name as 'group_name'
FROM {block_laixmo_modules} as M
LEFT JOIN {block_laixmo_module_outputs} as O
  ON M.module_key = O.module_key
LEFT JOIN {block_laixmo_output_groups} as G
  ON G.output_key = CONCAT(M.module_key, '-', O.output_key)
LEFT JOIN {block_laixmo_output_targets} as T
  ON T.output_key = CONCAT(M.module_key, '-', O.output_key)
WHERE
  O.output_type LIKE '{$type}' AND
  O.module_key LIKE '{$module}' AND
  T.target_name LIKE '{$target}' AND
  G.group_name LIKE '{$group}' ;
SQL;
        try {
            $records = $DB->get_records_sql($query);

            $records = array_map(function ($e) {
                return new output($e);
            }, $records);

        } catch (\Exception $ex) {
            $records = [];
        }

        return self::pack_list($records);
    }

    /**
     * Return all outputs that belong to the given module
     * @param string $module module to get outputs for
     * @return output[]
     */
    public static function get_for_module($module) : array {
        return self::get('%', '%', '%', $module);
    }

    /**
     * Removes duplicate outputs and combines outputs to a single one
     *
     * @param output[] $list
     * @return output[]
     */
    private static function pack_list($list) : array {
        $dlist = [];

        foreach ($list as $output) {
            if (isset($dlist[$output->get_key()])) {
                if (!in_array($output->targets[0], $dlist[$output->get_key()]->targets)) {
                    $dlist[$output->get_key()]->targets = array_merge($dlist[$output->get_key()]->targets, $output->targets);
                }
                if (!in_array($output->groups[0], $dlist[$output->get_key()]->groups)) {
                    $dlist[$output->get_key()]->groups = array_merge($dlist[$output->get_key()]->groups, $output->groups);
                }
            } else {
                $dlist[$output->get_key()] = $output;
            }
        }

        return array_values($dlist);
    }
}