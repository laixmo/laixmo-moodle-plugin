<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Proxy to the Laixmo Server.
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo;

defined('MOODLE_INTERNAL') || die();

require_once("{$CFG->libdir}/filelib.php");

use current_language;
use coding_exception;
use curl;
use moodle_exception;
use stdClass;

/**
 * Proxy Class
 * @package block_laixmo
 */
class proxy
{
    const REQUEST_GET = 'GET';
    const REQUEST_POST = 'POST';

    /**
     * Get content from a module
     *
     * @param $name string Module ID
     * @param $output string display type
     * @param $params string[] Query Parameters
     * @return response
     * @throws moodle_exception
     */
    public static function module($name, $output, $params) : response {
        return new response(self::request("/run/{$name}/{$output}", self::REQUEST_POST, $params));
    }

    /**
     * Get all available modules.
     *
     * @return string[]
     * @throws \dml_exception
     * @throws moodle_exception
     */
    public static function get_available_modules() : array {
        return self::request('/info')['modules'];
    }

    /**
     * Make HTTP-Request to the Laixmo Server
     *
     * @param string $target API-Endpoint
     * @param string $method Request method
     * @param array $params
     * @return stdClass Server response
     * @throws \dml_exception
     * @throws moodle_exception
     */
    public static function request($target, $method = self::REQUEST_GET, $params = []) : stdClass {

        $server = get_config('laixmo', 'Server_URL');
        $token = get_config('laixmo', 'Laixmo_TOKEN');

        $params['lang'] = explode('_', current_language())[0];

        if (substr($server, -1) == '/') {
            $server = substr($server, 0, -1);
        }

        $url = "${server}{$target}";

        $data = json_encode($params);

        $curl = new curl();

        $header = [
            "x-laixmo-token: $token",
            "accept: application/json"
        ];

        if ($method == self::REQUEST_POST) {
            $header[] = "Content-Type: application/json";
            $header[] = "Content-Length: " . strlen($data);
        }

        $curl->setHeader($header);

        $content = null;

        switch ($method) {
            case self::REQUEST_GET:
                $content = $curl->get($url);
                break;
            case self::REQUEST_POST:
                $content = $curl->post($url, $data);
                break;
            default:
                $content = null;
                break;
        }

        $content = \json_decode($content, true);

        $info = $curl->get_info();

        switch ($info['http_code']) {
            case 200:
                return $content;
            case 0:
                throw new moodle_exception(
                    'laixmo_connection',
                    'block_laixmo',
                    'https://git.rwth-aachen.de/LAIXMO/laixmo-moodle-plugin/wikis/home',
                    '',
                    'Moodle can not connect to the server.'
                );
            case 403:
                throw new moodle_exception(
                    'laixmo_auth',
                    'block_laixmo',
                    'https://git.rwth-aachen.de/LAIXMO/laixmo-moodle-plugin/wikis/home',
                    '',
                    'It is most likely that the auth token is not set correctly.'
                );
            case 404:
                throw new moodle_exception(
                    'laixmo_not_found',
                    'block_laixmo',
                    'https://git.rwth-aachen.de/LAIXMO/laixmo-moodle-plugin/wikis/home',
                    '',
                    'The laixmo server does not now the Module or Output key'
                );
            case 500:
                throw new moodle_exception(
                    'laixmo_server_error',
                    'block_laixmo',
                    'https://git.rwth-aachen.de/LAIXMO/laixmo-moodle-plugin/wikis/home',
                    '',
                    'Error while executing Laixmo Module'
                );
            default:
                throw new moodle_exception(
                    'laixmo_error',
                    'block_laixmo',
                    'https://git.rwth-aachen.de/LAIXMO/laixmo-moodle-plugin/issues',
                    $info['http_code'],
                    'This error should get documented and is not known yet.'
                );
        }

    }
}