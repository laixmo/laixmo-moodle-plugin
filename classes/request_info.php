<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Request Information
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo;

defined('MOODLE_INTERNAL') || die();

/**
 * Bundled information about the current request
 *
 * @see request
 * @package block_laixmo
 */
class request_info
{

    /**
     * Currently active output
     * @var output
     */
    private $active = null;

    /**
     * All available outputs
     * @var output[]
     */
    private $outputs = [];

    /**
     * Change the currently active output
     * @param $active output
     */
    public function set_active_output($active) {
        if ($this->active != null) {
            $this->active->set_selected(false);
        }

        $this->active = $active;
        $this->active->set_selected(true);
    }

    /**
     * Returns the currently active output
     * @return output
     */
    public function get_active_output() : output {
        return $this->active;
    }

    /**
     * Add Output to this RequestInfo
     * @param $out
     */
    public function add_output($out) {
        $this->outputs[] = $out;
    }

    /**
     * Get all Outputs inside of this Info
     * @param null $module
     * @return output[]
     */
    public function get_outputs($module = null) : array {
        if (!is_null($module)) {
            return array_values(array_filter($this->outputs, function ($e) use ($module) {
                return $e->get_module() == $module;
            }));
        }
        return $this->outputs;
    }

}