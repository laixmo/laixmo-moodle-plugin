<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Filter by Number
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo\local\filtering;

defined('MOODLE_INTERNAL') || die();

class filter_date extends filter
{

    private $range;

    public function __construct($key, $data) {
        parent::__construct($key, 'date');

        $this->range = isset($data['range']) ? array_map('floatval', $data['range']) : [];
    }

    /**
     * Return the needed template parameters
     * @return array
     */
    public function get_params() {
        return [
            'key' => $this->get_key(),
            'min' => isset($this->range['from']) ? $this->range['from'] : null,
            'max' => isset($this->range['to']) ? $this->range['to'] : null,
        ];
    }

    /**
     * @param $page
     */
    public function init($page) {
        $page->requires->js_call_amd('block_laixmo/filter_date-lazy', 'init', []);
    }
}