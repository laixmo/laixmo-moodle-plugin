<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Filter by Number
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo\local\filtering;

use moodle_page;

defined('MOODLE_INTERNAL') || die();

abstract class filter
{
    /**
     * @var string Filter Key
     */
    protected $key;

    /**
     * @var string Filter Type
     */
    protected $type;

    /**
     * Create new Filter.
     * @param string $key
     * @param string $type
     */
    public function __construct($key, $type) {
        $this->key = $key;
        $this->type = $type;
    }

    /**
     * Return the type of this filter
     *
     * @return string
     */
    public function get_type() {
        return $this->type;
    }

    /**
     * Return ID/Key/Name of tis filter
     *
     * @return string
     */
    public function get_key() {
        return $this->key;
    }

    /**
     * Return the needed template parameters
     * @return array
     */
    public abstract function get_params();

    /**
     * Initialize the Filter will be called when added to page
     * @param moodle_page $page
     * @return void
     */
    public function init($page) {
    }

}