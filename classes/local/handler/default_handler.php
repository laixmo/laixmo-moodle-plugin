<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Default request Handler
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo\local\handler;

defined('MOODLE_INTERNAL') || die();

use block_laixmo\output\laixmo_output;
use block_laixmo\output\html;
use context_system;

class default_handler extends handler {

    /**
     * @param $output
     * @param $params
     * @return Html
     */
    public function handle($output, $params) {
        return new Html('<h1>MISSING</h1>');
    }

    /**
     * @param $params
     * @return mixed
     * @throws \coding_exception
     * @throws \dml_exception
     * @throws \moodle_exception
     * @throws \require_login_exception
     */
    public function auth($params) {
        global $PAGE;

        if (isset($params['course'])) {
            require_course_login($params['course']);
        } else {
            require_login();
            $PAGE->set_context(context_system::instance());
        }

    }
}