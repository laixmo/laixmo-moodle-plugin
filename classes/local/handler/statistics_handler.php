<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handler for the general Statistics Page
 * (Restricted Access)
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo\local\handler;

use block_laixmo\local\user;
use block_laixmo\proxy;
use block_laixmo\request;
use block_laixmo\local\routing\route_parameter;
use block_laixmo_renderer;
use coding_exception;
use context_system;
use dml_exception;

defined('MOODLE_INTERNAL') || die();

/**
 * Handle Module requests
 *
 * @package block_laixmo\handler
 */
class statistics_handler extends handler
{

    /**
     * Handle the request
     *
     * @param block_laixmo_renderer $output
     * @param route_parameter $params
     * @return void
     * @throws \moodle_exception
     * @throws coding_exception
     */
    public function handle($output, $params) {
        global $PAGE;

        $user = user::get();

        $rparams = [
            'userid' => $user->get_id(),
            'courseids' => $user->get_courses()
        ];

        $PAGE->set_heading(get_string('global_statistics', 'block_laixmo'));

        $info = request::get_request_info($user->get_group(), $params->get('module'), $params->get('output'), 'global');
        $response = proxy::module($info->get_active_output()->get_module(), $info->get_active_output()->get_output(), $rparams);

        echo $output->render_global_module_selection(
            $info->get_outputs(),
            array_map(
                function ($e) {
                    return [
                        'id' => $e->id,
                        'name' => $e->shortname
                    ];
                },
                get_courses()
            )
        );

        $output->render_output_list($response->outputs);
    }

    /**
     * Apply required authentication
     *
     * @param $params
     * @return mixed
     * @throws dml_exception
     */
    public function auth($params) {
        global $PAGE;

        $context = context_system::instance();
        $PAGE->set_context($context);
    }
}