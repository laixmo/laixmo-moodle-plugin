<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *  Privacy Handler
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo\local\handler;

defined('MOODLE_INTERNAL') || die();

use block_laixmo\proxy;
use block_laixmo\local\routing\route_parameter;
use context_system;
use moodle_url;

class privacy_handler extends handler {

    /**
     * Handle the request
     *
     * @param $output
     * @param $params route_parameter
     * @return void
     * @throws \coding_exception
     * @throws \moodle_exception
     */
    public function handle($output, $params) {
        global $USER, $PAGE;

        $notify = '';

        // TODO: Create a request class.
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $consent = filter_input(INPUT_POST, 'consent', FILTER_SANITIZE_STRING);

            $consent = $consent != 'on' ? 1 : 0;

            $response = proxy::request('/data-privacy/' . $USER->id . "?consent={$consent}", proxy::REQUEST_POST);

            $notify = $response->status;
        }

        $response = proxy::request('/data-privacy/' . $USER->id);

        $PAGE->set_heading(get_string('privacy_title', 'block_laixmo'));

        if (!empty($notify)) {

            echo $output->render_from_template('block_laixmo/alert', [
                'type' => $notify === 'OK' ? 'success' : 'danger',
                'text' => get_string(($notify === 'OK' ? 'save_success' : 'save_error'), 'block_laixmo', $notify)
            ]);
        }

        echo $output->render_from_template('block_laixmo/privacy', [
            'action' => new moodle_url('/blocks/laixmo/index.php' . $this->route->get_url()),
            'text' => $response->text,
            'checked' => $response->value ? 'checked' : ''
        ]);
    }

    /**
     * Apply required authentication
     *
     * @param $params
     * @return mixed
     * @throws \coding_exception
     * @throws \dml_exception
     * @throws \moodle_exception
     * @throws \require_login_exception
     */
    public function auth($params) {
        global $PAGE;

        require_login();
        $PAGE->set_context(context_system::instance());
    }
}