<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Abstract request Handler
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


namespace block_laixmo\local\handler;

defined('MOODLE_INTERNAL') || die();

use block_laixmo\local\routing\route;
use block_laixmo\output;
use block_laixmo\output\laixmo_output;

/**
 * Class Handler
 * @package block_laixmo\handler
 */
abstract class handler {

    /**
     * @var route
     */
    protected $route;

    /**
     * Handler constructor.
     * @param $route
     */
    public function __construct($route) {
        $this->route = $route;
    }

    /**
     * Handle the request
     *
     * @param $params
     * @return laixmo_output
     */
    public abstract function handle($output, $params);

    /**
     * Apply required authentication
     *
     * @param $params
     * @return mixed
     */
    public abstract function auth($params);
}