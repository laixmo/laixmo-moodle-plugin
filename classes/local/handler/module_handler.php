<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Module Handler
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo\local\handler;

defined('MOODLE_INTERNAL') || die();

use block_laixmo\event\course_statistic_viewed;
use block_laixmo\local\user;
use block_laixmo\proxy;
use block_laixmo\request;
use block_laixmo\local\routing\route_parameter;
use block_laixmo\output\laixmo_output;
use coding_exception;
use context_course;

/**
 * Handle Module requests
 *
 * @package block_laixmo\handler
 */
class module_handler extends handler
{

    /**
     * Handle the request
     *
     * @param \block_laixmo_renderer $output
     * @param route_parameter        $params
     * @return laixmo_output
     * @throws \moodle_exception
     */
    public function handle($output, $params) {
        global $USER, $PAGE, $COURSE;

        $user = user::get();

        $infos = request::get_request_info($user->get_group(), $params->get('module'), $params->get('output'), 'course');

        $outputparams = [
            'courseids' => [(int)$params->get('course')],
            'userid' => $USER->id,
        ];

        $event = course_statistic_viewed::create([
            'courseid' => $COURSE->id,
            'userid' => $USER->id,
            'context' => context_course::instance($COURSE->id),
            'other' => [
                'module' => $infos->get_active_output()->get_module(),
                'output' => $infos->get_active_output()->get_output(),
            ],
        ]);

        $event->add_record_snapshot('course', $COURSE);
        $event->trigger();

        $response = proxy::module(
            $infos->get_active_output()->get_module(),
            $infos->get_active_output()->get_output(),
            $outputparams
        );

        echo $output->render_module_selection($params->get('course'), $infos->get_outputs());

        echo $output->render_filter($response->filter);
        $PAGE->requires->js_call_amd('block_laixmo/filter', 'init', [
            'course' => $COURSE->id,
            'module' => $infos->get_active_output()->get_module(),
            'output' => $infos->get_active_output()->get_output(),
            'params' => json_encode($outputparams),
        ]);

        if (isset($response->title)) {
            $PAGE->set_heading(get_string('details_title', 'block_laixmo', $response->title));
        } else {
            $PAGE->set_heading(get_string('details_title', 'block_laixmo', "{$infos->get_active_output()->get_module()}"));
        }

        $output->render_output_list($response->outputs);
    }

    /**
     * Apply required authentication
     *
     * @param $params route_parameter
     * @throws coding_exception
     */
    public function auth($params) {
        require_course_login($params->get('course'));
    }
}