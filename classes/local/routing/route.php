<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Request Route
 *
 * @package    block_laixmo
 * @copyright  2017 Marcel Behrmann
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo\local\routing;

defined('MOODLE_INTERNAL') || die();

use block_laixmo\local\handler\handler;

/**
 * Class representing a Route inside of the Routing mechanism
 *
 * @package block_laixmo\routing
 */
class route {

    /**
     * Path of the Route
     *
     * @var string
     */
    public $path;

    /**
     * Handler that will be executed when this route is active
     *
     * @var handler
     */
    public $handler;

    /**
     * Parameters of this Route
     *
     * @var route_parameter
     */
    public $params;

    /**
     * Path split into parts
     *
     * @var string[]
     */
    private $pathparts;

    /**
     * Route constructor.
     *
     * @param string $path Path that matches this Route
     * @param string $handler Handler to execute
     */
    public function __construct($path, $handler) {
        $this->path = $path;
        $this->handler = new $handler($this);

        $this->params = new route_parameter();

        $this->pathparts = explode('/', $this->path);
    }

    /**
     * Execute this Route
     */
    public function run() {
        global $PAGE;

        $this->handler->auth($this->params);

        $output = $PAGE->get_renderer('block_laixmo');

        // This buffering is needed to modify globals like heading etc.
        ob_start();
        $this->handler->handle($output, $this->params);

        $content = ob_get_clean();

        echo $output->header();
        echo $content;
        echo $output->footer();
    }

    /**
     * Check if the given url matches this route
     *
     * @param string $arguments Slash Arguments of the current Request
     * @return bool
     */
    public function does_match($arguments) : bool {
        $arguments = explode('/', $arguments);

        if (count($arguments) >= $this->get_needed_count() && count($arguments) <= count($this->pathparts)) {
            $count = count($arguments);

            for ($i = 0; $i < $count; $i++) {
                if (substr($this->pathparts[$i], 0, 1) === ':') {
                    if (!isset($arguments[$i])) {
                        return false;
                    }
                } else if (substr($this->pathparts[$i], 0, 1) === '?') {
                    continue;
                } else if ($this->pathparts[$i] !== $arguments[$i]) {
                    return false;
                }
            }
            return true;
        }

        return false;
    }

    private function get_needed_count() : int {
        $c = 0;
        $acount = count($this->pathparts);

        for ($i = 0; $i < $acount; $i++) {
            if (substr($this->pathparts[$i], 0, 1) != '?') {
                $c++;
            }
        }

        return $c;
    }

    /**
     * Get the Parameters from the given url
     *
     * @param string $url
     */
    public function parse_params($url) {
        $params = explode('/', $url);

        for ($i = 0; $i < count($params); $i++) {
            if ($this->pathparts[$i] != $params[$i]) {
                $this->params->add(trim($this->pathparts[$i], ':?'), $params[$i]);
            }
        }
    }

    /**
     * Return the url to this Route
     *
     * @return string
     */
    public function get_url() {

        $url = $this->path;

        foreach ($this->params as $key => $value) {
            $url = str_replace(':' . $key, $value, $url);
        }

        return $url;
    }
}