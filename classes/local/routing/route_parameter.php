<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Route Parameters
 *
 * @package    block_laixmo
 * @copyright  2017 Marcel Behrmann
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo\local\routing;

defined('MOODLE_INTERNAL') || die();

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * Represent Route Parameters
 *
 * @package block_laixmo\handler
 */
class route_parameter implements IteratorAggregate {

    /**
     * @var string[]
     */
    private $params;

    public function __construct() {
        $this->params = [];
    }

    /**
     * Add Element
     *
     * @param $key string
     * @param $value string
     */
    public function add($key, $value) {
        $this->params[$key] = $value;
    }

    /**
     * Checks if given key exists
     * @param $key
     * @return bool
     */
    public function contains($key) : bool {
        return isset($this->params[$key]);
    }

    /**
     * Checks if the given Parameter is Empty
     *
     * @param $key string
     * @return bool
     */
    public function is_empty($key) : bool {
        return !$this->contains($key) || $this->params[$key] == '';
    }

    /**
     * Get Value
     * @param $key string
     * @param string|null $default
     * @return string|null
     */
    public function get($key, $default = null) {
        return $this->contains($key) ? $this->params[$key] : $default;
    }

    /**
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @since 5.0.0
     */
    //@codingStandardsIgnoreLine
    public function getIterator() {
        return new ArrayIterator($this->params);
    }
}