<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Request router
 *
 * @package    block_laixmo
 * @copyright  2017 Marcel Behrmann
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo\local\routing;

defined('MOODLE_INTERNAL') || die();

use moodle_url;

/**
 * The Router is managing all routes and decides which one to execute
 *
 * @package block_laixmo\routing
 */
class router {
    /**
     * All registerd routes
     *
     * @var route[]
     */
    public $routes;

    /**
     * Router constructor.
     *
     * @param $routes route[] Routes to register
     */
    public function __construct($routes) {
        $this->routes = $routes;
    }

    /**
     * Return the currently active Route
     *
     * @param $fallback string Name of the handler class to execute when no Route matches
     * @return route
     */
    public function get_active_route($fallback) : route {
        if (isset($_SERVER['REQUEST_URI'])) {
            $request = new moodle_url($_SERVER['REQUEST_URI']);
        } else {
            $request = new moodle_url('/');
        }

        $slashargs = str_replace(
            $request->get_path(false),
            '',
            $request->get_path(true)
        );

        foreach ($this->routes as $route) {
            if ($route->does_match($slashargs)) {
                $route->parse_params($slashargs);
                return $route;
            }
        }
        return new route('/', $fallback);
    }
}