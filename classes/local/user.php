<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle User
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo\local;

defined('MOODLE_INTERNAL') || die();

/**
 * Wrapper around the currently logged in moodle user
 * @package block_laixmo
 */
class user
{
    const GROUP_TEACHER = 'teacher';
    const GROUP_STUDENT = 'student';

    private function __construct() {
        global $USER;

        $this->id = $USER->id;
    }

    private $id;

    private $group;

    private $courses;

    /**
     * Get the currently active user
     * @return user
     */
    public static function get() : user {
        $user = new user();
        $user->update_relevant_courses();
        return $user;
    }

    /**
     * Return courses in that the current user is manager
     * @return array
     */
    private function get_manager_courses() {
        global $DB;

        $query = <<<SQL
 SELECT `id`
 FROM {course}
 WHERE
     id IN (
         SELECT instanceid
         FROM {context}
         WHERE id IN (
             SELECT `contextid`
             FROM `{role_assignments}`
             WHERE
                 userid = {$this->id} AND
                 `roleid` <= 4
         ) AND
         contextlevel = 50
     ) AND
     `startdate` <= UNIX_TIMESTAMP() AND
     `enddate` > UNIX_TIMESTAMP()
SQL;
        try {
            $courses = $DB->get_records_sql($query);
            $courseids = array_keys($courses);
        } catch (\Exception $e) {
            $courseids = [];
        }

        return $courseids;
    }

    private function get_student_courses() {
        return array_keys(enrol_get_users_courses($this->id, true));
    }

    private function update_relevant_courses() {
        $courses = self::get_manager_courses();

        if (count($courses) > 0) {
            $this->group = self::GROUP_TEACHER;
            $this->courses = $courses;
        } else {
            $this->group = self::GROUP_STUDENT;
            $this->courses = self::get_student_courses();
        }
    }

    /**
     * @return mixed
     */
    public function get_courses() {
        return $this->courses;
    }

    /**
     * @return mixed
     */
    public function get_group() {
        return $this->group;
    }

    /**
     * @return mixed|object
     */
    public function get_id() {
        return $this->id;
    }
}