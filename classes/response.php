<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Laixmo Server Response
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_laixmo;

use block_laixmo\output\filter_missing;

defined('MOODLE_INTERNAL') || die();

/**
 * Laixmo Server Response
 *
 * @package block_laixmo
 */
class response
{
    /**
     * @var array
     */
    public $outputs;

    /**
     * @var array
     */
    public $filter;

    /**
     * @var string
     */
    public $title;

    /**
     * response constructor.
     * @param $data
     */
    public function __construct($data) {
        $this->outputs = $data['output'];
        $this->title = isset($data['title']) ? $data['title'] : null;

        $this->filter = isset($data['filter']) ? $data['filter'] : [];

        $this->filter = array_map(function ($e, $i) {
            $type = $e['type'];
            $filter = "block_laixmo\\local\\filtering\\filter_${type}";

            if (class_exists($filter)) {
                return new $filter($i, $e);
            } else {
                return new filter_missing($e['type']);
            }
        }, $this->filter, array_keys($this->filter));
    }

    /**
     * Turn the response into an AJAX response
     * @return array
     */
    public function to_ajax_response() : array {
        return [
            'output' => json_encode(json_encode($this->outputs, JSON_HEX_QUOT | JSON_HEX_TAG))
        ];
    }
}