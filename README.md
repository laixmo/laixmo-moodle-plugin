# Laixmo Moodle [![build status](https://git.rwth-aachen.de/LAIXMO/laixmo-moodle-plugin/badges/master/build.svg)](https://git.rwth-aachen.de/LAIXMO/laixmo-moodle-plugin/commits/master) [![coverage report](https://git.rwth-aachen.de/LAIXMO/laixmo-moodle-plugin/badges/master/coverage.svg)](https://git.rwth-aachen.de/LAIXMO/laixmo-moodle-plugin/commits/master)

Latest Build (unstable/nightly): [block_laixmo.zip](https://git.rwth-aachen.de/laixmo/laixmo-moodle-plugin/-/jobs/artifacts/master/raw/block_laixmo.zip?job=build)

Features
--------

- Display data from Laixmo Server

Requirements
------------

Moodle 3.0 or greater.  
Curl php extension must be enabled.

Running [Laixmo Server](https://git.rwth-aachen.de/LAIXMO/laixmo-server)

Installation
------------
Block can be installed simply from the zip.

The plugin also can be installed by installing the [composer](https://getcomposer.org/) package [laixmo/laixmo-moodle-plugin]()   

License
-------

Licensed under the [GNU GPL License](http://www.gnu.org/copyleft/gpl.html).