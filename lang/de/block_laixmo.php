<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Strings for the german Translation of this Plugin
 * Strings für die Deutsche übersetzung des Plugins
 *
 * @package    block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['pluginname'] = 'Statistiken (Laixmo)';

$string['block_title'] = 'Kurs Statistik';
$string['config_module'] = 'Module';
$string['config_output'] = 'Ausgaben';
$string['configure'] = 'Dieser Block wurde noch nicht konfiguriert.';
$string['description'] = 'Beschreibung';
$string['details'] = 'Details';
$string['details_title'] = 'Kursstatistik: {$a}';
$string['error:no_statistic'] = 'Keinestatistik verfügbar.';
$string['event:course_statistic_viewed'] = 'Kursstatistik angesehen.';
$string['event:course_statistic_viewed_desc'] = 'Der Nutzer mit der id \'{$a->user}\' hat sich eine Statistik für den Kurs mit der id \'{$a->course}\' angesehen.';
$string['filter'] = 'Filtern';
$string['id'] = 'ID';
$string['laixmo:seerestricted'] = 'See restricted Laixmo Statistics.';
$string['laixmo_auth'] = 'Authentifizierung mit dem Laixmo Server nicht erfolgreich.';
$string['laixmo_connection'] = 'Moodle kann keine Verbindung zu dem Laixmo Server aufbauen.';
$string['loading'] = 'Lade....';
$string['module_key'] = 'Modul Name';
$string['modules'] = 'Verfügbare Module';
$string['name'] = 'Name';
$string['not_available'] = 'Die ausgewählte Ausgabe ist nicht mehr verfügbar. Der Block muss neu konfiguriert werden.';
$string['outputs'] = 'Ausgaben';
$string['privacy'] = 'Privatssphäre';
$string['privacy:metadata:laixmo_server'] = 'Um den externen Laixmo Service einbinden zu können müssen Daten mit ihm ausgetauscht werden.';
$string['privacy:metadata:laixmo_server:userid'] = 'Um Personenbezogene Statsitiken anzubieten muss die userid übertragen werden.';
$string['privacy_settings'] = 'Privatssphäre Einstellungen';
$string['privacy_title'] = 'Privatssphäre Einstellungen';
$string['reload_modules'] = 'Verfügbare Module aktualisieren';
$string['reset'] = 'Zurücksetzen';
$string['save_error'] = 'Fehler beim speichern, bitte später erneut probieren.. ({$a})';
$string['save_success'] = 'Erfolgreich gespeichert. ({$a})';
$string['server_token_label'] = 'Laixmo Server Token';
$string['server_token_desc'] = 'Token für die Authentifizierung mit dem Server.';
$string['server_url_label'] = 'Server Adresse';
$string['server_url_desc'] = 'Adresse des Laixmo Servers';
$string['serverconfig'] = 'Laixmo Server Konfiguration';
$string['serverconfig_desc'] = 'Einstellungen die für die Kommunikation mit dem Laixmo Server relevant sind.';
$string['settings'] = 'Einstellungen';
$string['show'] = 'Anzeigen';
$string['global_statistics'] = 'Globale Statistken';
$string['submit'] = 'Senden';
$string['type_not_supported'] = 'Die aktuelle Version unterstützt den vom Server zurück gegebenen Anzeige-Typ nicht.';
