<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Strings for block_laixmo
 *
 * @package    block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['pluginname'] = 'Statistics (powered by Laixmo)';

$string['block_title'] = 'Course Statistics';
$string['config_module'] = 'Module';
$string['config_output'] = 'Output';
$string['configure'] = 'This block has not been configured yet.';
$string['description'] = 'Description';
$string['details'] = 'Details';
$string['details_title'] = 'Course Statistics: {$a}';
$string['error:no_statistic'] = 'No available statistic.';
$string['event:course_statistic_viewed'] = 'Course statistic viewed';
$string['event:course_statistic_viewed_desc'] = 'The user with id \'{$a->user}\' has looked at a statistic for course \'{$a->course}\'.';
$string['filter'] = 'Filter';
$string['id'] = 'ID';
$string['laixmo:seerestricted'] = 'See restricted Laixmo Statistics.';
$string['laixmo_auth'] = 'Authentication failed for the configured Laixmo Server.';
$string['laixmo_connection'] = 'Moodle can not connect to the configured Laixmo Instance.';
$string['loading'] = 'Loading....';
$string['module_key'] = 'Module Identifier';
$string['modules'] = 'Available Modules';
$string['name'] = 'Name';
$string['not_available'] = 'The choosen module or output is not supported anymore. The block needs to be reconfigured.';
$string['outputs'] = 'Outputs';
$string['privacy'] = 'Privacy';
$string['privacy:metadata:laixmo_server'] = 'In order to integrate the remote Laixmo services, user data needs to be exchanged with that service.';
$string['privacy:metadata:laixmo_server:userid'] = 'The userid is sent from Moodle to create statistics based on user. On receiving the userid is replaced with an anonymised id.';
$string['privacy_settings'] = 'Privacy Settings';
$string['privacy_title'] = 'Privacy Settings';
$string['reload_modules'] = 'Refresh available Modules';
$string['reset'] = 'Reset';
$string['save_error'] = 'Error while saving please try again later. ({$a})';
$string['save_success'] = 'Successfully Saved ({$a})';
$string['server_token_label'] = "Laixmo Server Token";
$string['server_token_desc'] = 'Token for Header authorization';
$string['server_url_label'] = 'Server Address';
$string['server_url_desc'] = 'Address of the LAIXMO-Server';
$string['serverconfig'] = 'Configuration Laixmo Server';
$string['serverconfig_desc'] = 'Configuration Laixmo Server';
$string['settings'] = 'Settings';
$string['show'] = 'Show';
$string['global_statistics'] = 'Global Statistics';
$string['submit'] = 'Submit';
$string['type_not_supported'] = 'The current version of the Block does not support the Content the server send back.';
