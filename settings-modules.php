<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Modules Settings Page.
 *
 * Settings Page to configure all available modules for the connected laixmo instance.
 *
 * @package     block_laixmo
 * @copyright   2018 Lehr- und Forschungsgebiet Ingenieurhydrologie - RWTH Aachen University
 * @author      Marcel Behrmann <behrmann@lfi.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once($CFG->libdir . '/adminlib.php');

defined('MOODLE_INTERNAL') || die;

require_login(0, false);

global $DB;

$PAGE->set_url(new moodle_url('/blocks/laixmo/settings-modules.php'));
admin_externalpage_setup('block_laixmo_modules');

$PAGE->set_title("{$SITE->shortname}: " . get_string('modules', 'block_laixmo'));

echo $OUTPUT->header();

echo '<h2>' . get_string('modules', 'block_laixmo') . '</h2>';

if (isset($_GET['action']) && $_GET['action'] == 'refresh') {

    \block_laixmo\module_manager::update_available();

    redirect(new moodle_url('/blocks/laixmo/settings-modules.php'));
} else {
    $modules = $DB->get_records('block_laixmo_modules');

    $table = new html_table();
    $table->head = [
        get_string('id', 'block_laixmo'),
        get_string('name', 'block_laixmo'),
        get_string('description', 'block_laixmo'),
        get_string('outputs', 'block_laixmo'),
    ];

    foreach ($modules as $module) {
        $outputs = \block_laixmo\output::get_for_module($module->module_key);

        $table->data[] = [
            $module->id,
            $module->name,
            $module->description,
            "<ul>" . array_reduce($outputs, function ($carry, $output) {
                return $carry . "<li>{$output->get_name()} ({$output->get_output()})</li>\r\n";
            }) . "</ul>"
        ];
    }

    echo '<br />';

    echo html_writer::link(
        new moodle_url('/blocks/laixmo/settings-modules.php?action=refresh'),
        get_string('reload_modules', 'block_laixmo'),
        [
            'class' => 'btn btn-primary',
            'style' => 'margin-bottom: 10px; margin-top: 10px;'
        ]
    );

    echo html_writer::table($table);

    echo $OUTPUT->footer();
}